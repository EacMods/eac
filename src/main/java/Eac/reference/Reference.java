package Eac.reference;

public class Reference
{

	public static final String MOD_ID = "eac";
	public static final String MOD_NAME = "Enders Aerial Cast";
	public static final String VERSION = "v2.0.0-pre1";
//    public static final String GUI_FACTORY_CLASS = "Eac.client.gui.GuiFactory";
    public static final String CLIENT_PROXY_CLASS = "Eac.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "Eac.proxy.ServerProxy";

}
